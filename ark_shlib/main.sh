
###-----------------------------------------------------------------------------
### @brief Gets the fullpath of the directory that the script resides.
### @note
###   The returned path is always from the script that is calling this
###   function, no matter if it's "sourced" or not.
### @returns
###   [echo] The fullpath of the function caller script. \n
###   [real] 0 if operation is succeeded, 1 otherwise.
### @NOTES:
###   This function needs to be here, because it's used in the initialization
###   scripts, nevertheless the real placement of it would be near the path
###   functions.
###
###   The previous approach of creating a "private" function and make this
###   call the "private" one happens to be problematic as the script directory
###   is reported differently.
###
###   For example, calling the "private" function thru the public one reports
###   the script dir of this file, not the file that source it.
###
###   Imagine that we have a file A.sh that source this file. on A.sh
###   we call ark_get_script_dir that by itself calls the "private" function,
###   it will report the path of this file, but calling the "private" function
###   directly reports the path of the A.sh.
###-----------------------------------------------------------------------------
function ark_get_script_dir()
{
    local SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[1]}")" && pwd)";
    echo "$SCRIPT_DIR";
}

###-----------------------------------------------------------------------------
function ark_get_program_path()
{
    ## @notice(stdmatt): This is needed because the MINGW64_NT-10.0-18362
    ## outputs a lot of non useful garbage when it can't find something.
    ## On GNU or MACOS the which(1) doesn't outputs nothing;...
    which "$1" > /dev/null 2>&1;
    if [ "$?" != "0" ]; then
        echo "";
        return -1;
    fi;

    echo $(which "$1");
    return 0;
}

###-----------------------------------------------------------------------------
function ark_get_abspath()
{
    echo $(ark_realpath "$1");
}

##
## OS Discovery functions
##
##------------------------------------------------------------------------------
function ARK_OS_NAME_MACOS()     { echo "macos";     }
function ARK_OS_NAME_GNU_LINUX() { echo "gnu_linux"; }
function ARK_OS_NAME_WINDOWS()   { echo "windows";   }
function ARK_OS_NAME_WSL()       { echo "wsl";       }

##------------------------------------------------------------------------------
function ark_os_is_macos()
{
    if [ "$(ark_get_os_simple_name)" == "$(ARK_OS_NAME_MACOS)" ]; then
        echo "$(ARK_OS_NAME_MACOS)";
        return 0;
    fi;
    echo "";
    return 1;
}

##------------------------------------------------------------------------------
function ark_os_is_gnu_linux()
{
    if [ "$(ark_get_os_simple_name)" == "$(ARK_OS_NAME_GNU_LINUX)" ]; then
        echo "$(ARK_OS_NAME_GNU_LINUX)";
        return 0;
    fi;
    echo "";
    return 1;
}

##------------------------------------------------------------------------------
function ark_os_is_wsl()
{
    if [ "$(ark_get_os_simple_name)" == "$(ARK_OS_NAME_WSL)" ]; then
        echo "$(ARK_OS_NAME_WSL)";
        return 0;
    fi;
    echo "";
    return 1;
}

##------------------------------------------------------------------------------
function ark_get_os_simple_name()
{
    ## @notice(stdmatt): This relies on the sourcing the correct
    ## file for each platform.
    _ark_impl_os_get_simple_name "$1";
}

##------------------------------------------------------------------------------
function _ark_discover_running_machine()
{
    local _UNAME_VALUE=$(uname);
    local SCRIPT_DIR="$(ark_get_script_dir)";

    ## macOS.
    if [ -n "$(echo "$_UNAME_VALUE" | grep "Darwin")" ]; then
        source "${SCRIPT_DIR}/macos.sh";

    ## GNU/Linux.
    elif [ -n "$(echo "$_UNAME_VALUE" | grep -i "Linux")" ]; then
        ## We might be running under WSL, so let's check it.
        local _IS_WSL=$(uname -r | grep -i "Microsoft");
        if [ -n "$_IS_WSL" ]; then
            source "${SCRIPT_DIR}/wsl.sh";
        else
            source "${SCRIPT_DIR}/gnu_linux.sh";
        fi;
    fi;
}

##
## GNU hacks...
##

###-----------------------------------------------------------------------------
### @brief Emulates the realpath in systems that don't have one.
### @note
###    In systems, like GNU, that has realpath, this function will
###    just forward the arguments to it.
function ark_realpath()
{
    ##
    ## We have the realpath(3), so just forward.
    local REALPATH=$(ark_get_program_path "realpath");
    if [ -n "$REALPATH" ]; then
        $REALPATH $@
        return;
    fi;

    ##
    ## We need to emulate it.
    ##   Thanks for Geoff [https://stackoverflow.com/a/18443300]
    local OLD_PWD="$PWD";

    cd "$(dirname "$1")"
    local LINK=$(readlink "$(basename "$1")");

    while [ "$LINK" ]; do
      cd "$(dirname "$LINK")";
      LINK=$(readlink "$(basename "$1")");
    done;

    local RESULT="$PWD/$(basename "$1")";

    cd "$OLD_PWD";
    echo "$RESULT";
}



##
## Script
##
_ark_discover_running_machine;
