#!/usr/bin/env bash

##----------------------------------------------------------------------------##
## Vars                                                                       ##
##----------------------------------------------------------------------------##
ARK_SHLIB_INSTALL_PATH="$HOME/.ark";


##----------------------------------------------------------------------------##
## Functions                                                                  ##
##----------------------------------------------------------------------------##
_get_script_dir()
{
    local SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[1]}")" && pwd)";
    echo "$SCRIPT_DIR";
    return 0;
}


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
##
## We're sourcing ourselves to let us use some library's functions.
_SCRIPT_PATH=$(_get_script_dir);
source "$_SCRIPT_PATH/ark_shlib/main.sh";

##
## Create thedirectory.
rm    -rf "$ARK_SHLIB_INSTALL_PATH";
mkdir -vp "$ARK_SHLIB_INSTALL_PATH";
cp    -vr "$_SCRIPT_PATH/ark_shlib" "$ARK_SHLIB_INSTALL_PATH";

##
## Self run to test if everything is ok.
if [ -f "$ARK_SHLIB_INSTALL_PATH/ark_shlib/main.sh" ]; then
    echo "Installation sucessful!";
else
    echo "Installation failed...";
fi;
